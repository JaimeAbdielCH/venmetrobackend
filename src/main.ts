import { NestFactory } from '@nestjs/core';
//import { Transport, MicroserviceOptions } from '@nestjs/microservices';
import { AppModule } from './app.module';

/* async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.TCP,
      options: {
        port: 3000,
        retryAttempts: 3,
        retryDelay: 155,
      },
    }
  );
  await app.listen(() => console.log('Venmetro Wallet Listening'));
}
bootstrap(); */

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}
bootstrap();

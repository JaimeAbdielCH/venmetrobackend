import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ModuleRegistro } from './registro/module';

@Module({
  imports: [ModuleRegistro],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

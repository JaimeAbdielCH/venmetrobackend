import { Controller, Get } from '@nestjs/common';

@Controller('registro')
export class RegistroController {
  @Get('/init')
  init(): string {
    return 'new token';
  }
}

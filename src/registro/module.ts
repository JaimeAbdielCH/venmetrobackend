import { Module } from '@nestjs/common';
import { RegistroController } from './controller';
import { RegistroService } from './service';

@Module({
  imports: [],
  controllers: [RegistroController],
  providers: [RegistroService],
})
export class ModuleRegistro {}
